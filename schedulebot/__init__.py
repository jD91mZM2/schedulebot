from discord.ext import commands as discmd
import discord
import asyncio
import os

from . import consts
from .commands import Commands
from .config import Config


def main():
    config = Config("schedulebot.toml")

    bot = discmd.Bot(command_prefix=consts.PREFIX)
    bot.add_cog(Commands(bot, config))

    @bot.listen()
    async def on_command_completion(ctx):
        try:
            await ctx.message.delete()
        except Exception:
            pass

    @bot.listen()
    async def on_command_error(ctx, error):
        try:
            await ctx.message.delete()
        except Exception:
            pass

        if isinstance(error, discmd.UserInputError):
            message = await ctx.send(embed=(
                discord.Embed(
                    title="You have made a grave mistake",
                    description=f"```{error}```",
                    colour=consts.COLOR_ERROR,
                ).set_footer(
                    text="What else could I expect from a bloody human, eh?",
                )
            ))
        elif isinstance(error, discmd.CommandNotFound):
            message = await ctx.send(embed=(
                discord.Embed(
                    title="Click here for instructional video",
                    description=(
                        "It appears that command didn't exist. Use "
                        f"`{consts.PREFIX}help` if you need a little help"
                    ),
                    url="https://youtu.be/dQw4w9WgXcQ",
                    colour=consts.COLOR_ERROR,
                ).set_footer(
                    text="Terribly sorry to trouble you, sir!",
                )
            ))

        await asyncio.sleep(5)
        await message.delete()

    bot.run(os.environ["SCHEDULEBOT_TOKEN"])
