from typing import Generic, TypeVar, Iterable, List
import heapq


T = TypeVar("T")


class HeapQueue(Generic[T]):
    def __init__(self):
        self.queue: list[T] = []

    def head(self) -> T:
        return self.queue[0]

    def push(self, value: T):
        """
        Push new queue entry to its correspending place in the queue
        """
        heapq.heappush(self.queue, value)

    def pop(self) -> T:
        """
        Pop first queue entry
        """
        return heapq.heappop(self.queue)

    def smallest(self, n: int) -> Iterable[T]:
        """
        Return n smallest entries in the queue
        """
        return heapq.nsmallest(n, self.queue)

    def drain(self) -> List[T]:
        """
        Clear the queue and return all previous internal data in the list
        """
        self.queue, previous = [], self.queue
        return previous

    def __len__(self) -> int:
        return len(self.queue)
