from collections import defaultdict
from dataclasses import dataclass
from datetime import datetime, timedelta
from discord.ext import commands
from discord.ext.commands import Cog
import asyncio
import discord
import functools
import parsedatetime

from .queue import HeapQueue
from . import consts


@dataclass
@functools.total_ordering
class QueueEntry:
    when: datetime
    task: asyncio.Task

    def __lt__(self, other):
        return self.when < other.when

    def __eq__(self, other):
        return self.when == other.when


def wrap(f):
    """
    Decorator for injecting current time and updated queue into the context
    """
    @functools.wraps(f)
    async def wrapper(self, ctx, *args, **kwargs):
        # Fetch time and queue
        ctx.now = datetime.now(self.config.tz)
        ctx.queue = self.queue[ctx.channel.id]

        # Clean past entries
        while ctx.queue:
            head = ctx.queue.head()
            if head.when > ctx.now:
                break
            ctx.queue.pop()

        result = await f(self, ctx, *args, **kwargs)

        return result

    return wrapper


def days_between(now, occurance):
    """
    Return the number of days left until an occurance, in a human-like way
    where you say it's "1 day left" when it might only be 23 hours left.
    """
    seconds_in_a_day = int(timedelta(days=1).total_seconds())
    days_now = int(now.timestamp()) // seconds_in_a_day
    days_then = int(occurance.timestamp()) // seconds_in_a_day
    return days_then - days_now


class Commands(Cog):
    def __init__(self, bot, config):
        self.bot = bot
        self.config = config

        self.queue = defaultdict(HeapQueue[QueueEntry])

        consts = parsedatetime.Constants()
        consts.StartHour = config.hour
        self.cal = parsedatetime.Calendar(constants=consts)

    async def sleep_until(self, time: datetime):
        """
        Sleep until a certain datetime. Returns True if it slept at all, False
        if the time is now or has already passed.
        """
        slept = False
        while True:
            remaining = time - datetime.now(self.config.tz)
            if remaining <= timedelta():
                return slept
            await asyncio.sleep(remaining.total_seconds())
            slept = True

    @commands.command()
    @wrap
    async def sched(self, ctx, *, datestr: str):
        """
        Schedules a new occurance that you will be reminded exists
        """
        source = ctx.now.replace(
            hour=self.config.hour,
            minute=self.config.minute,
            second=0,
            microsecond=0,
        )

        occurance, _ = self.cal.parseDT(
            datestr,
            sourceTime=source,
            tzinfo=self.config.tz,
        )

        date = occurance.date().isoformat()
        time = occurance.time().isoformat(timespec="minutes")

        if occurance < ctx.now:
            await ctx.send(embed=(
                discord.Embed(
                    title="Instance is in the past",
                    description=f"**{date}** isn't in my future",
                    colour=consts.COLOR_ERROR,
                ).set_footer(
                    text="I'm a lot of things, but not a time traveller",
                )
            ))
            return

        async def reminder():
            if await self.sleep_until(occurance - timedelta(hours=12)):
                await ctx.send(embed=(
                    discord.Embed(
                        title="Yes, we're still on",
                        description=(
                            "Unless any of you feel like "
                            "cancelling, it's happening in 12 hours!"
                        ),
                        colour=consts.COLOR_OK,
                    ).set_footer(
                        text=f"Cancel with {ctx.prefix}cancel"
                    )
                ))

            if await self.sleep_until(occurance - timedelta(minutes=5)):
                await ctx.send(embed=(
                    discord.Embed(
                        title="❗ Starting in 5 minutes ❗",
                        description="Hurry! Grab your snacks!",
                        colour=consts.COLOR_WARN,
                    ).set_footer(
                        text="Don't say I didn't warn you for Christ's sake"
                    )
                ))

            await self.sleep_until(occurance)
            await ctx.send("We're on! Where is @everyone?")

        task = self.bot.loop.create_task(reminder())

        ctx.queue.push(QueueEntry(occurance, task))

        days = days_between(ctx.now, occurance)
        plural = "" if days == 1 else "s"
        await ctx.send(embed=(
            discord.Embed(
                title=f"{days} day{plural} remaining 📅 🕐",
                description=f"Next occurance is at **{date}**, at **{time}**",
                colour=consts.COLOR_OK,
            ).set_footer(
                text="If I crash, I may not remind you. Please don't hurt me.",
            )
        ))

    @commands.command(aliases=["when"])
    @wrap
    async def next(self, ctx):
        """
        Prints when the next 5 occurances happen
        """
        if not ctx.queue:
            await ctx.send(embed=(
                discord.Embed(
                    title="No scheduled events",
                    description="You have no events scheduled. For now.",
                    colour=consts.COLOR_WARN,
                ).set_footer(
                    text="Oh boy, I sure hope I didn't mess this one up",
                )
            ))
            return

        head = ctx.queue.head()

        coming = "\n".join(map(
            lambda entry: (
                f"- **{entry.when.date()}** at "
                f"**{entry.when.time().isoformat(timespec='minutes')}**"
            ),
            ctx.queue.smallest(5)
        ))

        days = days_between(ctx.now, head.when)
        plural = "" if days == 1 else "s"
        await ctx.send(embed=(
            discord.Embed(
                title=f"{days} day{plural} remaining to next 📅 🕐",
                description=f"Scheduled occurances:\n\n{coming}",
                colour=consts.COLOR_OK,
            ).set_footer(
                text="If I crash, I may not remind you. Please don't hurt me.",
            )
        ))

    @commands.command()
    @wrap
    async def cancel(self, ctx):
        """
        Cancel the coming occurance - you will not be reminded of it
        """
        if not ctx.queue:
            await ctx.send(embed=(
                discord.Embed(
                    title="Cancelled your dentist's appointment",
                    description=(
                        "There was no time available to cancel. "
                        "So I cancelled your dentist's appointment for you. "
                        "You're welcome."
                    ),
                    colour=consts.COLOR_WARN,
                ).set_footer(
                    text="Have a pleasant day",
                )
            ))
            return

        occurance = ctx.queue.pop()
        occurance.task.cancel()

        date = occurance.when.date().isoformat()
        time = occurance.when.time().isoformat(timespec="minutes")

        await ctx.send(embed=(
            discord.Embed(
                title="Cancelled event",
                description=(
                    f"Occurance on **{date}** at **{time}** was cancelled"
                ),
                colour=consts.COLOR_WARN,
            ).set_footer(text=(
                "Feel free to use your newly gained time "
                "to have an existential crisis"
            ))
        ))

    @commands.command()
    @wrap
    async def clear(self, ctx):
        """
        Delete all occurances for this channel, making the bot very sad indeed.
        """
        prev = ctx.queue.drain()
        for occurance in prev:
            occurance.task.cancel()

        plural = "" if len(prev) == 1 else "s"
        await ctx.send(embed=(
            discord.Embed(
                title="Universe cancelled",
                description=(
                    f"{len(prev)} occurance{plural} removed. Did you do "
                    "this in a moment of rage, or was it a calculated "
                    "decision?"
                ),
                colour=consts.COLOR_WARN,
            ).set_image(url=(
                "https://i.ibb.co/cx3BR7B/You-monster-portal-2"
                "-30650838-1920-1200.jpg"
            )).set_footer(
                text="Because either way it makes me sad :(",
                icon_url=self.bot.user.avatar_url,
            )
        ))
