from dataclasses import dataclass
from datetime import tzinfo
import pytz
import toml


@dataclass
class Config:
    tz: tzinfo
    hour: int
    minute: int

    def __init__(self, filename):
        with open(filename) as f:
            config = toml.loads(f.read())

        self.tz = pytz.timezone(config["tz"])
        self.hour = config["default_hour"] or 0
        self.minute = config["default_minute"] or 0
