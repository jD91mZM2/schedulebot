{
  description = "My Awesome Python Project";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in rec {
      # `nix build`
      packages.schedulebot = let
        raw = pkgs.poetry2nix.mkPoetryApplication {
          projectDir = ./.;
        };
        withConfig = { config, }: pkgs.symlinkJoin {
          name = "schedulebot";
          paths = [ raw ];

          nativeBuildInputs = with pkgs; [ makeWrapper ];

          postBuild = ''
            # Add config
            mkdir -p "$out/share"
            cp "${config}" "$out/share/schedulebot.toml"

            # Wrap with config
            rm -r "$out/bin/schedulebot"
            makeWrapper "${raw}/bin/schedulebot" "$out/bin/schedulebot" --run "cd $out/share"
          '';
        };
        final = pkgs.makeOverridable withConfig {
          config = ./schedulebot.toml;
        };
      in final;
      defaultPackage = packages.schedulebot;

      # `nix run`
      apps.schedulebot = utils.lib.mkApp {
        drv = packages.schedulebot;
      };
      defaultApp = apps.schedulebot;

      # `nix develop`
      devShell = pkgs.mkShell {
        buildInputs = nixpkgs.lib.singleton (pkgs.poetry2nix.mkPoetryEnv {
          projectDir = ./.;
        });
        nativeBuildInputs = with pkgs; [ poetry ];
      };
    });
}
