# ScheduleBot

Super simple bot to remind you of silly things. It may or may not actually do
its job - I'm not storing any persistant state so rebooting the bot will clear
it. I don't give a crap.

## Permission

If you give the bot the ability to manage messages, it will delete your
commands that invoke the bot (so `!sched tomorrow` will disappear after you
send it). Otherwise, they stay put with no harm done.

## Commands

| Command  | Description                                                             |
|----------|-------------------------------------------------------------------------|
| `cancel` | Cancel the coming occurance - you will not be reminded of it            |
| `clear`  | Delete all occurances for this channel, making the bot very sad indeed. |
| `next`   | Prints when the next 5 occurances happen                                |
| `sched`  | Schedules a new occurance that you will be reminded exists              |
